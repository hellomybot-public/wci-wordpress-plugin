<?php
/**
 * In charge of generating the right content for the setting page
 */
function hmb_wci_settings_template_page() {
  ?>
  <div class="wrap">
  <h2>HMB Webchat Instance Configuration</h2>
  <p>This form allows you to quickly configure and activate an instance of the HelloMyBot webchat.</p>
  <br>
  <form method="post" action="options.php">
  <?php
  settings_fields("hmb-wci-opt-group");
  do_settings_sections("hmb-wci-opt-group");
  ?>
  <table class="form-table" role="presentation">
    <tbody>
    <tr>
      <th scope="row">
        <label for="hmb-wci-botcore-api-key">Botcore API Key</label>
      </th>
      <td>
        <input
          name="hmb-wci-botcore-api-key"
          type="text"
          id="hmb-wci-botcore-api-key"
          placeholder="XXXX-XXXX-XXXX-XXXX"
          class="regular-text"
          value="<?php echo esc_attr( get_option('hmb-wci-botcore-api-key') ); ?>"
          >
          <p class="description" id="tagline-description">The Botcore API Key that we provided to you.</p>
        </td>
      </tr>

       <tr>
        <th scope="row">
          <label for="hmb-wci-target">Target</label>
        </th>
        <td>
          <input
            name="hmb-wci-target"
            type="text"
            id="hmb-wci-target"
            placeholder="e.g. Content__container"
            class="regular-text"
            value="<?= esc_attr( get_option('hmb-wci-target') ); ?>"
          >
          <p class="description" id="tagline-description">The unique identifier of the html tag in which you wish to integrate the webchat. If needed.</p>
        </td>
      </tr>

      <tr>
        <th scope="row">
          <label for="hmb-wci-locale">Locale</label>
        </th>
        <td>
          <input
            name="hmb-wci-locale"
            type="text"
            id="hmb-wci-locale"
            placeholder="e.g. fr, en, de..."
            class="regular-text"
            value="<?= esc_attr( get_option('hmb-wci-locale') ); ?>"
          >
          <p class="description" id="tagline-description">Choice of the locale to be used. Optional.</p>
        </td>
      </tr>

      <tr>
        <th scope="row">Logs</th>
        <td>
          <fieldset>
            <legend class="screen-reader-text"><span>Logs</span></legend>
            <label for="hmb-wci-logs">
              <input
                name="hmb-wci-logs"
                type="checkbox"
                id="hmb-wci-logs"
                value="true"
                <?php checked( "true", get_option("hmb-wci-logs") ) ?>
              >
              Enable the logs
            </label>
          </fieldset>
        </td>
      </tr>
      </tbody>
    </table>
    <?php submit_button(); ?>
    </form>
  </div>
<?php } ?>


