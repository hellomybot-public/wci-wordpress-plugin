=== HMB Webchat Instance ===
Author: HelloMyBot
Contributors: Ben Lmsc
Tags: webchat, HelloMyBot, hmb
Tested up to: 5.6
Stable tag: trunk
License: GNU General Public License (GPL), version 3
License URI: https://gitlab.com/hellomybot-public/wci-wordpress-plugin/-/blob/master/LICENSE

Allows to quickly initialize the HelloMyBot webchat instance.

== Description ==
This plugin is designed to facilitate the implementation of the SaaS solution: the HelloMyBot webchat.

== Installation ==
Nothing special. You only need to activate the plugin and enter the Botcore API key in the associated settings page.
The Botcore API key should normally have been given to you before, if this is not the case please ask us for it.

== Changelog ==
Version 1.0.0
First Release