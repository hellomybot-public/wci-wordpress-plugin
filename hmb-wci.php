<?php
/**
 * Plugin Name: HMB Webchat Instance
 * Plugin URI: https://gitlab.com/hellomybot-public/wci-wordpress-plugin
 * Description: Allows to quickly initialize the HelloMyBot webchat instance
 * Version: 1.0.0
 * Author: HelloMyBot
 * Author URI: https://hellomybot.io/
 * License: GNU General Public License (GPL), version 3
 * License URI: https://gitlab.com/hellomybot-public/wci-wordpress-plugin/-/blob/master/LICENSE
 */

/**
 * Plugin menu configuration
 */
function hmb_wci_menu() {
  add_menu_page(
    "HMB Webchat Instance Configuration",
    "HMB WCI Settings",
    "manage_options",
    "hmb-wci-configuration",
    "hmb_wci_options"
  );
  add_action("admin_init", "hmb_wci_register_settings");
}

/**
 * Allows the injection of the hmb wci script with the right attributes
 */
function hmb_wci_script_injection() {
  $botcore_api_key = get_option("hmb-wci-botcore-api-key");
  if (!$botcore_api_key) return;
  ?>
    <script
      src="https://botcore.hellomybot.io/bundle"
      key="<?= $botcore_api_key ?>"
      id="HMB"
      <?
      $locale = get_option("hmb-wci-locale");
       if($locale) {
         echo "locale='$locale'";
       }
      ?>
      <?
      $target = get_option("hmb-wci-target");
      if($target) {
        echo "target='$target'";
      }
      ?>
      <?
      $logs = get_option("hmb-wci-logs");
      if($logs) {
        echo "log='$logs'";
      }
      ?>
    >
    </script>
  ";
  <?php
}

/**
 * Plugin options configuration
 */
function hmb_wci_options() {
  if (!current_user_can("manage_options")) {
    wp_die(__("You do not have sufficient permissions to access this page."));
  }
  include_once "includes/hmb-wci-form-template.php";
  if (function_exists("hmb_wci_settings_template_page")) {
    hmb_wci_settings_template_page();
  }
  else {
    echo "Error";
  }
}


/**
 * Plugin settings configuration
 */
function hmb_wci_register_settings()
{
  $option_group_name = "hmb-wci-opt-group";
  register_setting($option_group_name, "hmb-wci-botcore-api-key");
  register_setting($option_group_name, "hmb-wci-target");
  register_setting($option_group_name, "hmb-wci-locale");
  register_setting($option_group_name, "hmb-wci-logs");
}

add_action("admin_menu", "hmb_wci_menu"); // add the setting page to the admin menu
add_action("wp_footer", "hmb_wci_script_injection"); // inject the hmb wci script into the wp footer